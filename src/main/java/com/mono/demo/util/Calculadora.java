package com.mono.demo.util;

public class Calculadora {
	
	private double result;
	
//	public Calculadora() {
//	}
	
	public double suma(double num1, double num2) {
		result = num1+num2;
		return result;
	}
	public double resta(double num1, double num2) {
		result = num1-num2;
		return result;
	}
	public double multipicacion(double num1, double num2) {
		result = num1*num2;
		return result;
	}
	public double division(double num1, double num2) throws GenericException {
		if( num2 > 0 ) {
			result = num1/num2;
		}else {
			throw new GenericException("Error de División entre cero");							
		}
		return result;
	}
	public double getResult() {
		return result;
	}
}
