package com.mono.demo;

import static org.assertj.core.api.Assertions.not;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.mono.demo.util.Calculadora;
import com.mono.demo.util.GenericException;

public class CalculadoraTest {

	private final static Logger log = Logger.getLogger(CalculadoraTest.class);
	private Calculadora c;
	private GenericException ex;

	@Test
	public void calculadora() {
		log.info("Test: Suma de doubles.");
		log.info("Num1: " + 2.0);
		log.info("Num2: " + 5.0);
		c = new Calculadora();
		assertThat(c.suma(2.0, 5.0), is((7.0)));
		log.info("Result: " + c.getResult());
	}

	@Test
	public void multipicacion() {
		log.info("Test: multipicacion de doubles.");
		log.info("Num1: " + 2.0);
		log.info("Num2: " + 5.0);
		c = new Calculadora();
		assertThat(c.multipicacion(2.0, 5.0), is((10.0)));
		log.info("Result: " + c.getResult());
	}

	@Test
	public void resta() {
		log.info("Test: resta de doubles.");
		log.info("Num1: " + 2.0);
		log.info("Num2: " + 5.0);
		c = new Calculadora();
		assertThat(c.resta(15.0, 5.0), is((10.0)));
		log.info("Result: " + c.getResult());
	}

	@Test
	public void division() throws GenericException {
		log.info("Test: division de doubles.");
		log.info("Num1: " + 2.0);
		log.info("Num2: " + 5.0);
		Calculadora c = new Calculadora();
		assertThat(c.division(20.0, 5.0), is((4.0)));
		log.info("Result: " + c.getResult());
	}

	@Test(expected = GenericException.class)
	public void division2() throws GenericException {
		log.info("Test: Suma de doubles.");
		log.info("Num1: " + 2.0);
		log.info("Num2: " + 5.0);
		c = new Calculadora();
		c.division(20.0, 0.0);
	}

	@Before
	public void before() {
		c = null;
		ex = null;
	}

	@After
	public void after() {
		c = null;
		log.info("Ends\n");
	}
}
